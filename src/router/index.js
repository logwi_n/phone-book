import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/employee/:id',
    name: 'employee',
    component: () => import('../views/Employee.vue')
  },
  {
    path: '/*',
    name: 'Error',
    component: () => import('../views/Error.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  store
})

router.beforeEach((to, from, next) => {
  const dataIsLoad = store.getters.loaded
  const name = to['name']
  if (name === 'employee') {
    const id = to.params['id']
    if (!dataIsLoad) {
      const storeInit = store.dispatch('getEmployees')
      storeInit.then(() => {
        const empl = store.getters.employeeById(id)
        if (empl === undefined) next('/'); else next()
      })
    } else {
      const empl = store.getters.employeeById(id)
      if (empl === undefined) next('/'); else next()
    }
  } else {
    if (!dataIsLoad) store.dispatch('getEmployees').then(() => next()); else next()
  }
})

export default router
