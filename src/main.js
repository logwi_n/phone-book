import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import resource from 'vue-resource'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'vue-material-design-icons/styles.css'

Vue.config.productionTip = false
Vue.http.options.root = 'https://randomuser.me/api/'

new Vue({
  router,
  store,
  resource,
  render: h => h(App)
}).$mount('#app')
