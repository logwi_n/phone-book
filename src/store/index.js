import Vue from 'vue'
import Vuex from 'vuex'
import employees from './employees'
import shared from './shared'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { employees, shared }
})
