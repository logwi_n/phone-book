import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export default {
  state: {
    employees: [],
    resource: null
  },
  mutations: {
    FETCH_EMPLOYEES(state, payload) {
      state.employees = payload
    }
  },
  actions: {
    async getEmployees({ commit }) {
      const url = '?results=12&nat=gb&inc=name,phone,id,picture,dob,email'
      this.resource = await Vue.resource(url)
      return this.resource.get()
        .then(responce => responce.json())
        .then(object => object.results)
        .then(payload => {
          commit('FETCH_EMPLOYEES', payload)
          commit('SET_LOADED', true)
        })
        .catch((error) => {
          alert(error.statusText + error.bodyText)
        })
    }
  },
  getters: {
    employees(state) {
      return state.employees
    },
    employeeById(state) {
      return adId => {
        return state.employees[parseInt(adId) - 1]
      }
    }
  }
}
