export default {
  state: {
    loaded: false
  },
  mutations: {
    SET_LOADED(state, payload) {
      state.loaded = payload
    }
  },
  getters: {
    loaded(state) {
      return state.loaded
    }
  }
}
